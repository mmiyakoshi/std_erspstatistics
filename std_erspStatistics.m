function varargout = std_erspStatistics(varargin)
% STD_ERSPSTATISTICS MATLAB code for std_erspStatistics.fig
%      STD_ERSPSTATISTICS, by itself, creates a new STD_ERSPSTATISTICS or raises the existing
%      singleton*.
%
%      H = STD_ERSPSTATISTICS returns the handle to a new STD_ERSPSTATISTICS or the handle to
%      the existing singleton*.
%
%      STD_ERSPSTATISTICS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in STD_ERSPSTATISTICS.M with the given input arguments.
%
%      STD_ERSPSTATISTICS('Property','Value',...) creates a new STD_ERSPSTATISTICS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before std_erspStatistics_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to std_erspStatistics_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help std_erspStatistics

% Last Modified by GUIDE v2.5 10-Jul-2016 14:10:02

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @std_erspStatistics_OpeningFcn, ...
                   'gui_OutputFcn',  @std_erspStatistics_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before std_erspStatistics is made visible.
function std_erspStatistics_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to std_erspStatistics (see VARARGIN)

% Import STUDY and ALLEEG
STUDY  = evalin('base','STUDY');
ALLEEG = evalin('base','ALLEEG');
STUDY = std_readersp(STUDY, ALLEEG, 'design', STUDY.currentdesign, 'clusters', 2:length(STUDY.cluster));
assignin('base', 'STUDY', STUDY);

% Combine the condition names without using cell
tmpVariable = STUDY.design(STUDY.currentdesign).variable(1,1).value;
for n = 1:length(tmpVariable)
    if iscell(tmpVariable{n})
        tmpCell = tmpVariable{n};
        tmpString = '';
        for m = 1:length(tmpCell)
            tmpString = [tmpString '_' tmpCell{m}];
        end
        tmpString = tmpString(2:end);
        tmpVariable{n} = tmpString;
    end
end
handles.variable = tmpVariable;

% Obtain condition names
groupAndCondition = {STUDY.design(STUDY.currentdesign).variable.label};
if     any(groupAndCondition{1,1})
    groupAndCondition11 = STUDY.design(STUDY.currentdesign).variable(1,1).value;
elseif any(groupAndCondition{1,2})
    groupAndCondition12 = STUDY.design(STUDY.currentdesign).variable(1,2).value;
end

% Set up popupmenu
if exist('groupAndCondition11', 'var')
    set(handles.condition1Popupmenu, 'string', cell2mat(groupAndCondition11'));
    set(handles.condition2Popupmenu, 'string', cell2mat(groupAndCondition11'));
else
    set(handles.condition1Popupmenu, 'enable', 'off');
    set(handles.condition2Popupmenu, 'enable', 'off');
end
if exist('groupAndCondition12', 'var')
    set(handles.group1Popupmenu, 'string', cell2mat(groupAndCondition12'));
    set(handles.group2Popupmenu, 'string', cell2mat(groupAndCondition12'));
else
    set(handles.group1Popupmenu, 'enable', 'off');
    set(handles.group2Popupmenu, 'enable', 'off');
end

% Enter default baseline period
set(handles.baselineEdit, 'String', num2str([min(STUDY.cluster(1,3).ersptimes) 0]))

% Choose default command line output for std_erspStatistics
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes std_erspStatistics wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = std_erspStatistics_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in clusterList.
function clusterList_Callback(hObject, eventdata, handles)
% hObject    handle to clusterList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns clusterList contents as cell array
%        contents{get(hObject,'Value')} returns selected item from clusterList

%%%%%%%%%%%%%%%%%%%%%%
%%% Show scalp map %%%
%%%%%%%%%%%%%%%%%%%%%%
STUDY  = evalin('base','STUDY');
ALLEEG = evalin('base','ALLEEG');
currentCluster = get(handles.clusterList,'value')+1;
axes(handles.topoCentroidPlot);
std_topoplot(STUDY,ALLEEG,'clusters',currentCluster,'figure','off');

%%%%%%%%%%%%%%%%%%%%%%%%
%%% Show dipole plot %%%
%%%%%%%%%%%%%%%%%%%%%%%%

% Clear dipole axes
cla(handles.axialAxes,    'reset')
cla(handles.sagittalAxes, 'reset')
cla(handles.coronalAxes,  'reset')

axes(handles.axialAxes)
std_dipplot(STUDY,ALLEEG, 'clusters', currentCluster, 'mode', 'apart', 'figure', 'off');
view(0,90)

axes(handles.sagittalAxes)
std_dipplot(STUDY,ALLEEG, 'clusters', currentCluster, 'mode', 'apart', 'figure', 'off');
view(90,0)

axes(handles.coronalAxes)
std_dipplot(STUDY,ALLEEG, 'clusters', currentCluster, 'mode', 'apart', 'figure', 'off');
view(0,0)

% Fix the background color
set(gcf, 'color', [0.66 0.76 1]);

% Select data for group and condition from Popupmenu
groupAndCondition = {STUDY.design(STUDY.currentdesign).variable.label};
group1Idx     = get(handles.group1Popupmenu, 'value');
group2Idx     = get(handles.group2Popupmenu, 'value');
condition1Idx = get(handles.condition1Popupmenu, 'value');
condition2Idx = get(handles.condition2Popupmenu, 'value');
if any(strcmp(groupAndCondition, 'group') | strcmp(groupAndCondition, 'session'))
    groupOrSessionOrderIdx = find(strcmp(groupAndCondition, 'group') | strcmp(groupAndCondition, 'session'));
    if     groupOrSessionOrderIdx == 1
        data1 = STUDY.cluster(1,currentCluster).erspdata{group1Idx,condition1Idx};
        data2 = STUDY.cluster(1,currentCluster).erspdata{group2Idx,condition2Idx};
        data1Base = STUDY.cluster(1,currentCluster).erspbase{group1Idx,condition1Idx};
        data2Base = STUDY.cluster(1,currentCluster).erspbase{group2Idx,condition2Idx};
    elseif groupOrSessionOrderIdx == 2
        data1 = STUDY.cluster(1,currentCluster).erspdata{condition1Idx,group1Idx};
        data2 = STUDY.cluster(1,currentCluster).erspdata{condition2Idx,group2Idx};
        data1Base = STUDY.cluster(1,currentCluster).erspbase{condition1Idx,group1Idx};
        data2Base = STUDY.cluster(1,currentCluster).erspbase{condition2Idx,group2Idx};
    end
else % This is not tested, could be wrong!
    data1 = STUDY.cluster(1,currentCluster).erspdata{condition1Idx,group1Idx};
    data2 = STUDY.cluster(1,currentCluster).erspdata{condition2Idx,group2Idx};
    data1Base = STUDY.cluster(1,currentCluster).erspbase{condition1Idx,group1Idx};
    data2Base = STUDY.cluster(1,currentCluster).erspbase{condition2Idx,group2Idx};
end
latency = STUDY.cluster(1,currentCluster).ersptimes;
freqs   = STUDY.cluster(1,currentCluster).erspfreqs;

% Process baseline
baselineLatency = str2num(get(handles.baselineEdit, 'String'));
baselineIdx = find(latency >= baselineLatency(1) & latency <= baselineLatency(2));
data1 = bsxfun(@plus, permute(data1, [1 3 2]), double(data1Base));
data1 = permute(data1, [1 3 2]);
data1 = bsxfun(@minus, data1, mean(data1(:,baselineIdx,:),2));
data2 = bsxfun(@plus, permute(data2, [1 3 2]), double(data2Base));
data2 = permute(data2, [1 3 2]);
data2 = bsxfun(@minus, data2, mean(data2(:,baselineIdx,:),2));

% Prepare plot titles
group1Items     = cellstr(get(handles.group1Popupmenu, 'String'));
group1String    = group1Items{get(handles.group1Popupmenu, 'Value')};
condition1Items = cellstr(get(handles.condition1Popupmenu, 'String'));
condition1String   = condition1Items{get(handles.condition1Popupmenu, 'Value')};
group2Items     = cellstr(get(handles.group2Popupmenu, 'String'));
group2String       = group2Items{get(handles.group2Popupmenu, 'Value')};
condition2Items = cellstr(get(handles.condition2Popupmenu, 'String'));
condition2String   = condition2Items{get(handles.condition2Popupmenu, 'Value')};

% Prepare axis labels
freqIdx(1) = find(freqs>2,1);
freqIdx(2) = find(freqs>4,1);
freqIdx(3) = find(freqs>8,1);
freqIdx(4) = find(freqs>13,1);
freqIdx(5) = find(freqs>30,1);
freqIdx(6) = find(freqs>50,1);

% Prepare color scale
colorScaleLimit = [-max(abs(vec([mean(data1,3) mean(data2,3)]))) max(abs(vec([mean(data1,3) mean(data2,3)])))];

%%%%%%%%%%%%%%%%%%%%%%%
%%% Plot Condition1 %%%
%%%%%%%%%%%%%%%%%%%%%%%
axes(handles.condition1Axes);
imagesc(latency, 1:length(freqs), mean(data1,3), colorScaleLimit); axis xy

% Set axis
set(gca, 'YTick', freqIdx, 'YTickLabel', {'2' '4' '8' '13' '30' '50'});
set(get(handles.condition1Axes,'YLabel'), 'String', 'Frequency (Hz)');

% Set title
if     strcmp(group1String, '(None)')
    title(sprintf('%s', condition1String));
elseif strcmp(condition1String, '(None)')
    title(sprintf('%s', group1String));
else
    title(sprintf('%s, %s', group1, condition1String));
end
set(findall(handles.condition1Axes,'-property','FontSize'),'FontSize',14);

% Draw event-onset vertical line
hold on
if any(latency==0)
    line([0 0], ylim, 'Color', [0 0 0], 'LineWidth', 3);
end

%%%%%%%%%%%%%%%%%%%%%%%
%%% Plot Condition2 %%%
%%%%%%%%%%%%%%%%%%%%%%%
axes(handles.condition2Axes);
imagesc(latency, 1:length(freqs), mean(data2,3), colorScaleLimit); axis xy

% Set axis
set(gca, 'YTick', freqIdx, 'YTickLabel', {'2' '4' '8' '13' '30' '50'});
set(get(handles.condition2Axes,'XLabel'), 'String', 'Latency (ms)');
set(get(handles.condition2Axes,'YLabel'), 'String', 'Frequency (Hz)');

% Set title
if     strcmp(group2String, '(None)')
    title(sprintf('%s', condition2String));
elseif strcmp(condition2String, '(None)')
    title(sprintf('%s', group2String));
else
    title(sprintf('%s, %s', group2, condition2String));
end
set(findall(handles.condition2Axes,'-property','FontSize'),'FontSize',14);

% Draw event-onset vertical line
hold on
if any(latency==0)
    line([0 0], ylim, 'Color', [0 0 0], 'LineWidth', 3);
end

%%%%%%%%%%%%%%%%%%%%%%%
%%% Plot Difference %%%
%%%%%%%%%%%%%%%%%%%%%%%
axes(handles.differenceAxes);
imagesc(latency, 1:length(freqs), mean(data1,3)-mean(data2,3), colorScaleLimit); axis xy
originalPosition = get(handles.differenceAxes, 'position');

% Show color bar
colorbarHandle = colorbar;
set(handles.differenceAxes, 'position', originalPosition);
set(get(colorbarHandle, 'title'), 'String', 'Delta dB')

% Set axis
set(gca, 'YTick', freqIdx, 'YTickLabel', {'2' '4' '8' '13' '30' '50'});
set(get(handles.differenceAxes,'XLabel'), 'String', 'Latency (ms)');

% Set title
title(sprintf('Difference (1-2)'));
set(findall(handles.differenceAxes,'-property','FontSize'),'FontSize',14);

% Draw event-onset vertical line
hold on
if any(latency==0)
    line([0 0], ylim, 'Color', [0 0 0], 'LineWidth', 3);
end

% Store data1 and data2 to this axis
erspData.data1 = data1;
erspData.data2 = data2;
set(handles.differenceAxes, 'UserData', erspData)

% Refresh cluster size report
set(handles.clusterSizeReportText, 'String', '')

% Choose default command line output for std_erspStatistics
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);



% --- Executes on button press in startStatisticsButton.
function startStatisticsButton_Callback(hObject, eventdata, handles)
% hObject    handle to startStatisticsButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Launch GUI
[fileName, pathName] = uiputfile('erspStatistics.mat');
if ~any(fileName)
    return
end

% Display process start
disp(sprintf('\n'))
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
disp('%%% Bootstrap Statistics Started. %%%')
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
disp(sprintf('\n'))

% Obtain STUDY and ALLEEG info
STUDY  = evalin('base','STUDY');
ALLEEG = evalin('base','ALLEEG');
clusterLabels = get(handles.clusterList, 'String');
latency = STUDY.cluster(1,2).ersptimes;
freqs   = STUDY.cluster(1,2).erspfreqs;
baselineLatency = str2num(get(handles.baselineEdit, 'String'));
baselineIdx = find(latency >= baselineLatency(1) & latency <= baselineLatency(2));
groupAndCondition = {STUDY.design(STUDY.currentdesign).variable.label};
group1Idx     = get(handles.group1Popupmenu, 'value');
group2Idx     = get(handles.group2Popupmenu, 'value');
condition1Idx = get(handles.condition1Popupmenu, 'value');
condition2Idx = get(handles.condition2Popupmenu, 'value');
    
% Prepare save data package
numIteration = 10000;
clusterERSP  = struct('tScore', [], 'pValue', [], 'maxStat', []);
tScoreTotal  = zeros(length(STUDY.cluster(1,3).erspfreqs), length(STUDY.cluster(1,3).ersptimes), length(clusterLabels));
pValueTotal  = zeros(length(STUDY.cluster(1,3).erspfreqs), length(STUDY.cluster(1,3).ersptimes), length(clusterLabels));
maxStatTotal = zeros(numIteration, 5, length(clusterLabels));

% Start the main loop
processTimeList = zeros(length(clusterLabels),1);
for clusterLoopIdx = 1:length(clusterLabels)
    
    % Select data for group and condition from Popupmenu
    currentCluster = clusterLoopIdx+1;
    if any(strcmp(groupAndCondition, 'group') | strcmp(groupAndCondition, 'session'))
        groupOrSessionOrderIdx = find(strcmp(groupAndCondition, 'group') | strcmp(groupAndCondition, 'session'));
        if     groupOrSessionOrderIdx == 1
            data1 = STUDY.cluster(1,currentCluster).erspdata{group1Idx,condition1Idx};
            data2 = STUDY.cluster(1,currentCluster).erspdata{group2Idx,condition2Idx};
            data1Base = STUDY.cluster(1,currentCluster).erspbase{group1Idx,condition1Idx};
            data2Base = STUDY.cluster(1,currentCluster).erspbase{group2Idx,condition2Idx};
        elseif groupOrSessionOrderIdx == 2
            data1 = STUDY.cluster(1,currentCluster).erspdata{condition1Idx,group1Idx};
            data2 = STUDY.cluster(1,currentCluster).erspdata{condition2Idx,group2Idx};
            data1Base = STUDY.cluster(1,currentCluster).erspbase{condition1Idx,group1Idx};
            data2Base = STUDY.cluster(1,currentCluster).erspbase{condition2Idx,group2Idx};
        end
    else % This is not tested, could be wrong!
        data1 = STUDY.cluster(1,currentCluster).erspdata{condition1Idx,group1Idx};
        data2 = STUDY.cluster(1,currentCluster).erspdata{condition2Idx,group2Idx};
        data1Base = STUDY.cluster(1,currentCluster).erspbase{condition1Idx,group1Idx};
        data2Base = STUDY.cluster(1,currentCluster).erspbase{condition2Idx,group2Idx};
    end
    
    % Process baseline
    data1 = bsxfun(@plus, permute(data1, [1 3 2]), double(data1Base));
    data1 = permute(data1, [1 3 2]);
    data1 = bsxfun(@minus, data1, mean(data1(:,baselineIdx,:),2));
    data2 = bsxfun(@plus, permute(data2, [1 3 2]), double(data2Base));
    data2 = permute(data2, [1 3 2]);
    data2 = bsxfun(@minus, data2, mean(data2(:,baselineIdx,:),2));
    
    % Perform statistics
    input1 = reshape(data1, [size(data1,1)*size(data1,2) size(data1,3)]);
    input2 = reshape(data2, [size(data2,1)*size(data2,2) size(data2,3)]);
    [tScore, pValue, maxSurroStat_u0_u1_u10_u50_u100] = bootstrapGFWER(input1', input2', numIteration);
    tScore = reshape(tScore, [size(data1,1) size(data1,2)]);
    pValue = reshape(pValue, [size(data1,1) size(data1,2)]);
    
    tScoreTotal(:,:,clusterLoopIdx) = tScore;
    pValueTotal(:,:,clusterLoopIdx) = pValue;
    maxStatTotal(:,:,clusterLoopIdx) = maxSurroStat_u0_u1_u10_u50_u100;
    
    % Display time elapsed
    timeElapsed = toc;
    processTimeList(clusterLoopIdx) = timeElapsed;
    meanValueSoFar = mean(processTimeList(processTimeList>0));
    timeRemaining = (length(clusterLabels)-clusterLoopIdx)*meanValueSoFar;
    if     timeRemaining >= 3600
        disp(sprintf('%.0f/%.0f done, %.1f hours remaining.',   clusterLoopIdx, length(clusterLabels), timeRemaining/3600));
    elseif timeRemaining >  60
        disp(sprintf('%.0f/%.0f done, %.1f minutes remaining.', clusterLoopIdx, length(clusterLabels), timeRemaining/60));
    else
        disp(sprintf('%.0f/%.0f done, %.0f seconds remaining.', clusterLoopIdx, length(clusterLabels), timeRemaining));
    end
end

% Store the final results
clusterERSP.tScore = tScoreTotal;
clusterERSP.pValue = pValueTotal;
clusterERSP.maxStat = maxStatTotal;
clusterERSP.baseline = baselineLatency;
    group1Label     = cellstr(get(handles.group1Popupmenu, 'string'));
    condition1Label = cellstr(get(handles.condition1Popupmenu, 'string'));
    group2Label     = cellstr(get(handles.group2Popupmenu, 'string'));
    condition2Label = cellstr(get(handles.condition2Popupmenu, 'string'));
    str = [group1Label{group1Idx} ', ' ...
        condition1Label{condition1Idx} ' - '...
        group2Label{group2Idx} ', '...
        condition2Label{condition2Idx}];
clusterERSP.conditions = str;

% Save the final results
save([pathName fileName], 'clusterERSP');

% Display process end
disp(sprintf('\n'))
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
disp('%%% Bootstrap Statistics Ended. %%%')
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
disp(sprintf('\n'))


% % Obtain statistical threshold
% statTypeIdx = get(handles.selectStatisticsMethod, 'value');
%     % 1 = uncorrected p<0.05
%     % 2 = FWER p<0.05
%     % 3 = FWER p<0.01
%     % 4 = GFWER p<0.05
%     % 5 = GFWER p<0.01
% 
% switch statTypeIdx
%     case 1
%         mask = pValue<0.05;
%     case 2
%         mask = tScore<prctile(statTailsMax, 2.5) | tScore>prctile(statTailsMax, 97.5);
%     case 3
%         mask = tScore<prctile(statTailsMax, 0.5) | tScore>prctile(statTailsMax, 99.5);        
%     case 4
%         mask = tScore<prctile(statTails005, 2.5) | tScore>prctile(statTails005, 97.5);    
%     case 5
%         mask = tScore<prctile(statTails001, 0.5) | tScore>prctile(statTails001, 99.5);
% end
%         
% % Return if no significance
% if ~any(mask(:))
%     disp('Nothing survived.')
%     return
% end
% 
% % Calculate cluster size
% [blobIdxMap, numBlobs] = bwlabeln(mask);
% blobSize = zeros(numBlobs-1,1);
% for n = 1:numBlobs
%     blobSize(n) = sum(sum(blobIdxMap == n));
% end
% 
% % Mask clusters smaller than 1% of map area
% smallBlobMask = blobSize < ceil(size(mask,1)*size(mask,2)*0.005); % 0.005
% smallBlobIdx = find(smallBlobMask);
% cleanedMask = blobIdxMap;
% for n = 1:length(smallBlobIdx)
%     cleanedMask(cleanedMask==smallBlobIdx(n))=0;
% end
% cleanedBlobIdx = bwlabeln(logical(cleanedMask));
% 
% % Plot contour
% axes(handles.differenceAxes)
% hold on
% latency = STUDY.cluster(1,currentCluster).ersptimes;
% freqs   = STUDY.cluster(1,currentCluster).erspfreqs;
% [~,contHandle] = contour(latency, 1:length(freqs), logical(cleanedMask), 1, 'k');
% set(contHandle, 'LineWidth', 2)
% 
% % Plot significant cluster
% figure
% imagesc(latency, 1:length(freqs), cleanedBlobIdx); axis xy;
% freqIdx(1) = find(freqs>2,1);
% freqIdx(2) = find(freqs>4,1);
% freqIdx(3) = find(freqs>8,1);
% freqIdx(4) = find(freqs>13,1);
% freqIdx(5) = find(freqs>30,1);
% freqIdx(6) = find(freqs>50,1);
% set(gca, 'YTick', freqIdx, 'YTickLabel', {'2' '4' '8' '13' '30' '50'});
% set(get(gca,'XLabel'), 'String', 'Latency (ms)');
% set(get(gca,'YLabel'), 'String', 'Frequency (Hz)');
% title('Blob index');
% hold on
% if any(latency==0)
%     line([0 0], ylim, 'Color', [0 0 0], 'LineWidth', 3);
% end
% colorbar
% set(findall(gcf, '-property', 'fontsize'), 'fontsize', 16)
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%% Extract subject info: Condition 1 %%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% group1Idx     = get(handles.group1Popupmenu, 'value');
% group2Idx     = get(handles.group2Popupmenu, 'value');
% condition1Idx = get(handles.condition1Popupmenu, 'value');
% condition2Idx = get(handles.condition2Popupmenu, 'value');
% setinds = STUDY.cluster(currentCluster).setinds{condition1Idx,group1Idx};
% datasetIdx = cell2mat({STUDY.design(STUDY.currentdesign).cell(setinds).dataset})';
% subjectNames = {ALLEEG(1,datasetIdx).subject}';
% tmpData = reshape(data1, [size(data1,1)*size(data1,2) size(data1,3)]);
% output = zeros(length(setinds), max(cleanedBlobIdx(:)));
% for n = 1:max(cleanedBlobIdx(:))
%     linearIdx = find(cleanedBlobIdx==n);
%     meanTmpBlob = mean(tmpData(linearIdx,:))';
%     output(:,n) = meanTmpBlob;
% end
% [uniqueId, firstUniqueIdx, uniqueIdx] = unique(datasetIdx);
% subjectMean1 = zeros(length(uniqueId), max(cleanedBlobIdx(:)));
% for n = 1:length(uniqueId)
%     tmpIdx = find(datasetIdx==uniqueId(n));
%     tmpMean = mean(output(tmpIdx,:),1);
%     subjectMean1(n,:) = tmpMean;
% end
% condition1Output = [subjectNames(firstUniqueIdx) num2cell(subjectMean1)];
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%% Extract subject info: Condition 2 %%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% setinds = STUDY.cluster(currentCluster).setinds{condition2Idx,group2Idx};
% datasetIdx = cell2mat({STUDY.design(STUDY.currentdesign).cell(setinds).dataset})';
% subjectNames = {ALLEEG(1,datasetIdx).subject}';
% tmpData = reshape(data2, [size(data2,1)*size(data2,2) size(data2,3)]);
% output = zeros(length(setinds), max(cleanedBlobIdx(:)));
% for n = 1:max(cleanedBlobIdx(:))
%     linearIdx = find(cleanedBlobIdx==n);
%     meanTmpBlob = mean(tmpData(linearIdx,:))';
%     output(:,n) = meanTmpBlob;
% end
% [uniqueId, firstUniqueIdx, uniqueIdx] = unique(datasetIdx);
% subjectMean2 = zeros(length(uniqueId), max(cleanedBlobIdx(:)));
% for n = 1:length(uniqueId)
%     tmpIdx = find(datasetIdx==uniqueId(n));
%     tmpMean = mean(output(tmpIdx,:),1);
%     subjectMean2(n,:) = tmpMean;
% end
% condition2Output = [subjectNames(firstUniqueIdx) num2cell(subjectMean2)];
% 
% % Report final results
% combinedOutput = [condition1Output; condition2Output];
% clusterNameList = get(handles.clusterList, 'String');
% currentClusterName = clusterNameList{currentCluster-1};
% disp(sprintf('\n\nIndividual subject ERSP for each blob from %s', currentClusterName));
% disp(combinedOutput)



% --- Executes on selection change in condition2Popupmenu.
function condition2Popupmenu_Callback(hObject, eventdata, handles)
% hObject    handle to condition2Popupmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns condition2Popupmenu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from condition2Popupmenu


% --- Executes during object creation, after setting all properties.
function condition2Popupmenu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to condition2Popupmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in group2Popupmenu.
function group2Popupmenu_Callback(hObject, eventdata, handles)
% hObject    handle to group2Popupmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns group2Popupmenu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from group2Popupmenu


% --- Executes during object creation, after setting all properties.
function group2Popupmenu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to group2Popupmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in condition1Popupmenu.
function condition1Popupmenu_Callback(hObject, eventdata, handles)
% hObject    handle to condition1Popupmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns condition1Popupmenu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from condition1Popupmenu


% --- Executes during object creation, after setting all properties.
function condition1Popupmenu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to condition1Popupmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in group1Popupmenu.
function group1Popupmenu_Callback(hObject, eventdata, handles)
% hObject    handle to group1Popupmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns group1Popupmenu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from group1Popupmenu


% --- Executes during object creation, after setting all properties.
function group1Popupmenu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to group1Popupmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function baselineEdit_Callback(hObject, eventdata, handles)
% hObject    handle to baselineEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of baselineEdit as text
%        str2double(get(hObject,'String')) returns contents of baselineEdit as a double


% --- Executes during object creation, after setting all properties.
function baselineEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to baselineEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function clusterList_CreateFcn(hObject, eventdata, handles)
% hObject    handle to clusterList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

clusterList = evalin('base', 'STUDY.cluster');
allClusterName = {clusterList.name}';
allClusterName = allClusterName(2:end);
set(hObject, 'String', allClusterName);

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in numBootstrap.
function numBootstrap_Callback(hObject, eventdata, handles)
% hObject    handle to numBootstrap (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns numBootstrap contents as cell array
%        contents{get(hObject,'Value')} returns selected item from numBootstrap


% --- Executes during object creation, after setting all properties.
function numBootstrap_CreateFcn(hObject, eventdata, handles)
% hObject    handle to numBootstrap (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in selectStatisticsMethod.
function selectStatisticsMethod_Callback(hObject, eventdata, handles)
% hObject    handle to selectStatisticsMethod (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns selectStatisticsMethod contents as cell array
%        contents{get(hObject,'Value')} returns selected item from selectStatisticsMethod


% --- Executes during object creation, after setting all properties.
function selectStatisticsMethod_CreateFcn(hObject, eventdata, handles)
% hObject    handle to selectStatisticsMethod (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in makeBackgroundWhitecheckbox.
function makeBackgroundWhitecheckbox_Callback(hObject, eventdata, handles)
% hObject    handle to makeBackgroundWhitecheckbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of makeBackgroundWhitecheckbox
if get(hObject,'Value')==1
    set(gcf, 'color', [1 1 1])
else
    set(gcf, 'color', [0.66 0.76 1])
end



function pValueEdit_Callback(hObject, eventdata, handles)
% hObject    handle to pValueEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of pValueEdit as text
%        str2double(get(hObject,'String')) returns contents of pValueEdit as a double


% --- Executes during object creation, after setting all properties.
function pValueEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pValueEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in loadStatsPushbutton.
function loadStatsPushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to loadStatsPushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[filename, filepath] = uigetfile('*.mat', 'Choose precomputed ERSP');
load([filepath filename]); % load clusterERSP
set(handles.loadStatsPushbutton, 'UserData', clusterERSP);

disp('Precomputed ERSP statisitcs loaded successfully.')

% Choose default command line output for std_erspStatistics
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);



% --- Executes on button press in applyCorrectedMaskPushbutton.
function applyCorrectedMaskPushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to applyCorrectedMaskPushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Check if precompute is done
if isempty(get(handles.loadStatsPushbutton, 'UserData'))
    disp('Load precomputed statistics first.')
    return
end

% Obtain STUDY and ALLEEG info
STUDY  = evalin('base','STUDY');
ALLEEG = evalin('base','ALLEEG');
clusterLabels = get(handles.clusterList, 'String');

% Obtain currenct cluster selection
statsClusterIdx = get(handles.clusterList,'value');

% Load precomputed data
stats = get(handles.loadStatsPushbutton, 'UserData');
pValue  = stats.pValue(:,:,statsClusterIdx);
tScore  = stats.tScore(:,:,statsClusterIdx);
maxStat = stats.maxStat(:,:,statsClusterIdx);
maxStatIdx = get(handles.selectStatisticsMethod, 'value'); % 0 (FWER), 1, 10, 50, 100

storedData = get(handles.differenceAxes, 'UserData');
data1 = storedData.data1;
data2 = storedData.data2;

% Obtain p-value threshold
pThreshold = str2num(get(handles.pValueEdit, 'String'));

% Calculate FWER/GFWER mask--note that this maxStat test is one-sided (right tail).
selectedMaxSurro = maxStat(:,maxStatIdx);
criticalT = prctile(selectedMaxSurro, 100*(1-pThreshold));

sortedPvalue = sort(pValue(:), 'ascend');
switch maxStatIdx
    case 1 % u == 0
        acceptedMask = zeros(size(pValue));
    case 2 % u == 1
        acceptedPvalue = sortedPvalue(1);
        acceptedMask = pValue<=acceptedPvalue;
    case 3 % u == 10
        acceptedPvalue = sortedPvalue(10);
        acceptedMask = pValue<=acceptedPvalue;
    case 4 % u == 50
        acceptedPvalue = sortedPvalue(50);
        acceptedMask = pValue<=acceptedPvalue;
    case 5 % u == 100
        acceptedPvalue = sortedPvalue(100);
        acceptedMask = pValue<=acceptedPvalue;
end

uncorrectedMask = pValue<=pThreshold;
maxStatMask     = abs(tScore)>=criticalT;
mask   = acceptedMask | (uncorrectedMask & maxStatMask);

% If no significant result
if ~any(mask(:))
    disp('Nothing survived.')
    set(handles.clusterSizeReportText, 'String', '')
    try
        if isfield(handles, 'contourHandle1')
            delete(handles.contourHandle1)
            delete(handles.contourHandle2)
            delete(handles.contourHandle3)
        end
    end
    return
end
    
% Calculate cluster size
[blobIdxMap, numBlobs] = bwlabeln(mask);
blobSize = zeros(numBlobs,1);
for n = 1:numBlobs
    blobSize(n) = sum(sum(blobIdxMap == n));
end

% Mask clusters k < 20
smallBlobMask = blobSize < str2num(get(handles.clusterSizeEdit, 'String'));
smallBlobIdx = find(smallBlobMask);
cleanedMask = blobIdxMap;
for n = 1:length(smallBlobIdx)
    cleanedMask(cleanedMask==smallBlobIdx(n))=0;
end
[cleanedBlobIdxMap, numBlobs] = bwlabeln(logical(cleanedMask));
blobSize = zeros(numBlobs,1);
for n = 1:numBlobs
    blobSize(n) = sum(sum(cleanedBlobIdxMap == n));
end

% If no significant result
if ~any(cleanedBlobIdxMap(:))
    disp('Nothing survived.')
    set(handles.clusterSizeReportText, 'String', '')
    try
        if isfield(handles, 'contourHandle1')
            delete(handles.contourHandle1)
            delete(handles.contourHandle2)
            delete(handles.contourHandle3)
        end
    end
    return
end

% Plot contour
axes(handles.differenceAxes)
hold on
latency = STUDY.cluster(1,statsClusterIdx+1).ersptimes;
freqs   = STUDY.cluster(1,statsClusterIdx+1).erspfreqs;
try
    if isfield(handles, 'contourHandle1')
        delete(handles.contourHandle1)
        delete(handles.contourHandle2)
        delete(handles.contourHandle3)
    end
end
[~,contHandle1] = contour(handles.condition1Axes, latency, 1:length(freqs), logical(cleanedBlobIdxMap), 1, 'k');
[~,contHandle2] = contour(handles.condition2Axes, latency, 1:length(freqs), logical(cleanedBlobIdxMap), 1, 'k');
[~,contHandle3] = contour(handles.differenceAxes, latency, 1:length(freqs), logical(cleanedBlobIdxMap), 1, 'k');
set(contHandle1, 'LineWidth', 2)
set(contHandle2, 'LineWidth', 2)
set(contHandle3, 'LineWidth', 2)
handles.contourHandle1 = contHandle1;
handles.contourHandle2 = contHandle2;
handles.contourHandle3 = contHandle3;

% Plot significant cluster
figure
imagesc(latency, 1:length(freqs), cleanedBlobIdxMap); axis xy;
freqIdx(1) = find(freqs>2,1);
freqIdx(2) = find(freqs>4,1);
freqIdx(3) = find(freqs>8,1);
freqIdx(4) = find(freqs>13,1);
freqIdx(5) = find(freqs>30,1);
freqIdx(6) = find(freqs>50,1);
set(gca, 'YTick', freqIdx, 'YTickLabel', {'2' '4' '8' '13' '30' '50'});
set(get(gca,'XLabel'), 'String', 'Latency (ms)');
set(get(gca,'YLabel'), 'String', 'Frequency (Hz)');
title('Blob index');
hold on
if any(latency==0)
    line([0 0], ylim, 'Color', [0 0 0], 'LineWidth', 3);
end
colorbar
set(findall(gcf, '-property', 'fontsize'), 'fontsize', 16)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Extract subject info: Condition 1 %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
group1Idx     = get(handles.group1Popupmenu, 'value');
group2Idx     = get(handles.group2Popupmenu, 'value');
condition1Idx = get(handles.condition1Popupmenu, 'value');
condition2Idx = get(handles.condition2Popupmenu, 'value');
setinds = STUDY.cluster(statsClusterIdx+1).setinds{condition1Idx,group1Idx};
datasetIdx = cell2mat({STUDY.design(STUDY.currentdesign).cell(setinds).dataset})';
subjectNames = {ALLEEG(1,datasetIdx).subject}';
tmpData = reshape(data1, [size(data1,1)*size(data1,2) size(data1,3)]);
output = zeros(length(setinds), max(cleanedBlobIdxMap(:)));
for n = 1:max(cleanedBlobIdxMap(:))
    linearIdx = find(cleanedBlobIdxMap==n);
    meanTmpBlob = mean(tmpData(linearIdx,:))';
    output(:,n) = meanTmpBlob;
end
[uniqueId, firstUniqueIdx, uniqueIdx] = unique(datasetIdx);
subjectMean1 = zeros(length(uniqueId), max(cleanedBlobIdxMap(:)));
for n = 1:length(uniqueId)
    tmpIdx = find(datasetIdx==uniqueId(n));
    tmpMean = mean(output(tmpIdx,:),1);
    subjectMean1(n,:) = tmpMean;
end
condition1Output = [subjectNames(firstUniqueIdx) num2cell(subjectMean1)];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Extract subject info: Condition 2 %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
setinds = STUDY.cluster(statsClusterIdx+1).setinds{condition2Idx,group2Idx};
datasetIdx = cell2mat({STUDY.design(STUDY.currentdesign).cell(setinds).dataset})';
subjectNames = {ALLEEG(1,datasetIdx).subject}';
tmpData = reshape(data2, [size(data2,1)*size(data2,2) size(data2,3)]);
output = zeros(length(setinds), max(cleanedBlobIdxMap(:)));
for n = 1:max(cleanedBlobIdxMap(:))
    linearIdx = find(cleanedBlobIdxMap==n);
    meanTmpBlob = mean(tmpData(linearIdx,:))';
    output(:,n) = meanTmpBlob;
end
[uniqueId, firstUniqueIdx, uniqueIdx] = unique(datasetIdx);
subjectMean2 = zeros(length(uniqueId), max(cleanedBlobIdxMap(:)));
for n = 1:length(uniqueId)
    tmpIdx = find(datasetIdx==uniqueId(n));
    tmpMean = mean(output(tmpIdx,:),1);
    subjectMean2(n,:) = tmpMean;
end
condition2Output = [subjectNames(firstUniqueIdx) num2cell(subjectMean2)];

% Report final results
combinedOutput = [condition1Output; condition2Output];
clusterNameList = get(handles.clusterList, 'String');
currentClusterName = clusterNameList{statsClusterIdx};
disp(sprintf('\n\nIndividual subject ERSP for each blob from %s', currentClusterName));
str = '';
for n = 1:length(blobSize)
    str = [str sprintf('Blob%2.0f, %2.0f; ', n, blobSize(n))];
end
disp(sprintf('Blob Index and size: %s', str))
disp(combinedOutput)

blobSizeSorted = sort(blobSize, 'descend'); 
str = '';
for n = 1:length(blobSize)
    str = [str sprintf('%.0f, ', blobSizeSorted(n))];
end
str = [str(1:end-2) '.'];
twoLineStr = sprintf('Total k = %.0f (for k>=20) \n%s ', sum(blobSize), str);
set(handles.clusterSizeReportText, 'String', twoLineStr)

% Choose default command line output for std_erspStatistics
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);



function clusterSizeEdit_Callback(hObject, eventdata, handles)
% hObject    handle to clusterSizeEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of clusterSizeEdit as text
%        str2double(get(hObject,'String')) returns contents of clusterSizeEdit as a double


% --- Executes during object creation, after setting all properties.
function clusterSizeEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to clusterSizeEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
